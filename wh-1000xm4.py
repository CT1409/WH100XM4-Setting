import bluetooth
import argparse
import re
import socket
from Message import Message, Message_Type

class BluetoothIO(socket.socket):
    def __init__(self, remote_addr, port, protocol = socket.BTPROTO_RFCOMM):
        super().__init__(family = socket.AF_BLUETOOTH, proto= protocol)
        self.__addr = remote_addr
        self.__channel = port
        self.sock_connected = False
        self.initiated = False
        self.send_seq = b'\x00'
        self.recv_seq = b'\x01'

    def connect(self):
        super().connect((self.__addr, self.__channel))
        self.sock_connected = True
        self.initiate()

    def initiate(self):
        super().send(Message(b'\x00', Message_Type.INIT_REQUEST).to_bytes())
        ret = self.recv(Message_Type.INIT_REPLY, expected_seq = self.recv_seq)
        super().send(Message(None, Message_Type.ACK).to_bytes())
        self.initiated = True
        self.__change_seq()

    def __change_seq(self, send_seq = None):
        if isinstance(send_seq, bytes):
            if send_seq == b'\x00' or send_seq == b'\x01':
                self.send_seq = send_seq
                self.recv_seq = (1 - int.from_bytes(self.send_seq)).to_bytes()
            else:
                raise ValueError("invalid sequence number")
        else:
            seq1 = self.recv_seq
            seq2 = self.send_seq
            self.send_seq = seq1
            self.recv_seq = seq2

    def recv(self, msgtype, retry = 2, expected_seq = b'\x00'):
        while retry > 0:
            data = [super().recv(1024)]
            if b'<>' in data[0]:
                sep = b'<'
                data = [i + sep for i in data[0].split(sep)[:-1]]
            for msg in data:
                msg = Message(msg)
                if msg.getSequencenumber() != expected_seq:
                    raise ValueError("wrong sequence number")
                if Message_Type.ACK == msg.getMsgType():
                    ACK = 1
                elif msg.getMsgType() == msgtype:
                    return msg
                else:
                    retry -= 1
        if ACK == 1:
            raise EnvironmentError("Headphone recieved message but ")
        else:
            raise EnvironmentError("No message")

    def getFirmwareVersion(self):
        if self.initiate:
            super().send(Message(b'\x02', Message_Type.FW_VERSION_GET, self.send_seq).to_bytes())
            ret = self.recv(Message_Type.FW_VERSION_REPLY, expected_seq = self.recv_seq)
            super().send(Message(None, Message_Type.ACK, self.send_seq).to_bytes())
            self.__change_seq()
            return ret.getPayload()[3:].decode('utf-8')
    
    # def getAmbientSoundControl(self):
    #     if self.initiate:
    #         super().send(Message(b'\x02', Message_Type.AMBIENT_SOUND_CONTROL_GET, self.send_seq).to_bytes())
    #         ret = self.recv(Message_Type.AMBIENT_SOUND_CONTROL_REPLY, expected_seq = self.recv_seq)
    #         super().send(Message(None, Message_Type.ACK, self.send_seq).to_bytes())
    #         self.__change_seq()
    #         return ret.getPayload()

    # def getTouchSensorSetting(self):
    #     if self.initiate:
    #         super().send(Message(b'\xd2', Message_Type.TOUCH_SENSOR_GET, self.send_seq).to_bytes())
    #         ret = self.recv(Message_Type.TOUCH_SENSOR_REPLY, expected_seq = self.recv_seq)
    #         super().send(Message(None, Message_Type.ACK, self.send_seq).to_bytes())
    #         self.__change_seq()
    #         return ret.getPayload()

    # def getSpeaktoChatConfig(self):
    #     if self.initiate:
    #         super().send(Message(b'\x05', Message_Type.SPEAK_TO_CHAT_CONFIG_GET, self.send_seq).to_bytes())
    #         ret = self.recv(Message_Type.SPEAK_TO_CHAT_CONFIG_REPLY, expected_seq = self.recv_seq)
    #         super().send(Message(None, Message_Type.ACK, self.send_seq).to_bytes())
    #         self.__change_seq()
    #         return ret.getPayload()



def isValidMac(address):
    if re.match("[0-9a-f]{2}([-:]?)[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$", address.lower()):
        return True
    else:
        return False

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('mac', metavar='mac-address', type=str, nargs=1, help='MAC-Address of WH-1000XM4')
    uuid = '96CC203E-5068-46AD-B32D-E316F5E069BA'
    args = parser.parse_args()
    mac = str(args.mac[0])
    if not isValidMac(mac):
        raise ValueError("invalid mac-address")
    service_matches = bluetooth.find_service(uuid=uuid, address=mac)[0]
    channel = service_matches['port']
    sock = BluetoothIO(mac, channel)
    try:
        sock.settimeout(5)
        sock.connect()
        print("Firmware Version: " + sock.getFirmwareVersion())
        sock.close()
    finally:
        sock.close()

if __name__ == "__main__":
    main()