from enum import Enum

class Message_Type(Enum):
    __CMD = b'\x0c'
    ACK = b'\x01'

    INIT_REQUEST = (__CMD, b'\x00')
    INIT_REPLY = (__CMD, b'\x01')

    FW_VERSION_GET = (__CMD, b'\x04')
    FW_VERSION_REPLY = (__CMD, b'\x05')

    AMBIENT_SOUND_CONTROL_GET = (__CMD, b'f')
    AMBIENT_SOUND_CONTROL_REPLY = (__CMD, b'g')

    # TOUCH_SENSOR_GET = (__CMD, b'\xd6')
    # TOUCH_SENSOR_REPLY = (__CMD, b'\xd7')

    SPEAK_TO_CHAT_CONFIG_GET = (__CMD, b'\xfa')
    SPEAK_TO_CHAT_CONFIG_REPLY = (__CMD, b'\xfb')


    def getCommandType(self):
        return self.value[0]

    def getPayloadType(self):
        try:
            return self.value[1]
        except:
            return None

class Message():
    __msgHeader = b'>'
    __msgTrailer = b'<'

    def __init__(self, data, msgtype = None, seq = b'\x00'):
        if isinstance(msgtype, Message_Type):
            if isinstance(seq, bytes):
                self.sequence_number = seq
            self.msgtype = msgtype
            self.payload_type = msgtype.getPayloadType()
            if data != None and self.payload_type != None: 
                self.cmdtype = msgtype.getCommandType()
                self.payload = self.payload_type + data
                self.payload_length = len(self.payload).to_bytes(4, byteorder="big")
            elif data == None and self.payload_type == None:
                self.cmdtype = msgtype.getCommandType().to_bytes()
                self.payload = None
                self.payload_length = int(0).to_bytes(4, byteorder="big")
            self.calchecksum()
        elif msgtype == None:
                if data[0].to_bytes() != self.__msgHeader: 
                    raise ValueError("Invalid header")
                if data[-1].to_bytes() != self.__msgTrailer:
                    raise ValueError("Invalid trailer")
                if data[1] == Message_Type.ACK.getCommandType():
                    self.msgtype = Message_Type.ACK
                    self.cmdtype = self.msgtype.getCommandType().to_bytes()
                    if int.from_bytes(data[3:7], byteorder = "big") == 0:
                        self.sequence_number = data[2].to_bytes()
                        self.payload_type = None
                        self.payload = None
                        self.payload_length = int(0).to_bytes(4, byteorder="big")
                        self.calchecksum()
                        if self.__checksum != data[-2].to_bytes():
                            raise ValueError("Invalid checksum")
                    else:
                        raise ValueError("ACK have Payload")
                else:
                    self.msgtype = Message_Type((data[1].to_bytes(),data[7].to_bytes()))
                    self.cmdtype = self.msgtype.getCommandType()
                    self.payload_type = self.msgtype.getPayloadType()
                    self.sequence_number = data[2].to_bytes()
                    self.payload = data[7:-2]
                    if len(self.payload) == int.from_bytes(data[3:7], byteorder = "big"):
                        self.payload_length = data[3:7]
                    self.calchecksum()

    def to_bytes(self):
        try:
            return self.__msgHeader + self.cmdtype + self.sequence_number + self.payload_length + self.payload + self.__checksum + self.__msgTrailer
        except:
            return self.__msgHeader + self.cmdtype + self.sequence_number + self.payload_length + self.__checksum + self.__msgTrailer

    def getPayloadType(self):
        return self.payload_type
    
    def getMsgType(self):
        return self.msgtype

    def getChecksum(self):
        return self.__checksum
    
    def getPayloadType(self):
        return self.payload_type
    
    def getPayload(self):
        return self.payload

    def getPayloadLength(self):
        return self.payload_length

    def getSequencenumber(self):
        return self.sequence_number

    def calchecksum(self):
        chksum = 0
        try:
            for i in (self.cmdtype + self.sequence_number + self.payload_length + self.payload):
                chksum += i & 255
        except:
            for i in (self.cmdtype + self.sequence_number + self.payload_length):
                chksum += i & 255
        chksum = chksum & 255
        self.__checksum = chksum.to_bytes()

    def isMsgType(msgtype):
        if self.msg == msgtype:
            return True
        else:
            return False
